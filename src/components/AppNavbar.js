import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import { Form, FormControl, Button } from "react-bootstrap";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../App.css";

export default function AppNavbar() {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/" className="mx-4">
        GizBytes
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={Link} to="/" exact>
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/courses" exact>
            Components
          </Nav.Link>
          <Form className="d-flex" id="navSearch">
            <FormControl
              type="search"
              placeholder="Search"
              className="mr-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Nav>
        <Nav className="justify-content-end mx-5" style={{ width: "100%" }}>
          <Nav.Link as={Link} to="/logout" exact>
            Logout
          </Nav.Link>
          <Nav.Link as={Link} to="/register" exact>
            Register
          </Nav.Link>
          <Nav.Link as={Link} to="/login" exact>
            Login
          </Nav.Link>
          <Nav.Link as={Link} to="/user" exact>
            <FontAwesomeIcon icon={faUser} size="md"></FontAwesomeIcon>
          </Nav.Link>
          <Nav.Link as={Link} to="/cart" exact>
            <FontAwesomeIcon icon={faShoppingCart} size="md"></FontAwesomeIcon>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
