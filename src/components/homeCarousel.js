import Carousel from "react-bootstrap/Carousel";
import image1 from "../images/mechatronics.jpg";
import image2 from "../images/Top_Raspberry_Pi_Mechatronics_Projects_1200x.jpg";
import "../App.css";

export default function homeCarousel() {
  return (
    <div style={{ display: "block" }}>
      <Carousel>
        <Carousel.Item interval={3000}>
          <img className="d-block w-100" src={image1} alt="One" id="img1" />
          <Carousel.Caption className="text-end">
            <h1>Label for first slide</h1>
            <p>Sample Text for Image One</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={3000}>
          <img className="d-block w-100" src={image2} alt="Two" id="img2" />
          <Carousel.Caption className="text-end">
            <h1>Label for second slide</h1>
            <p>Sample Text for Image Two</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
