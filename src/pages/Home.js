import { Fragment } from "react";
import Carousel from "../components/homeCarousel";

export default function Home() {
  return (
    <Fragment>
      <Carousel />
    </Fragment>
  );
}
